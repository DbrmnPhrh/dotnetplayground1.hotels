public class Hotel
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string HotelName { get; set; }
    public string HotelRegion { get; set; }
}
